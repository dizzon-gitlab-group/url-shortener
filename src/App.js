import React, { useState, useEffect } from "react";
import './App.css';
import Axios from "axios";

function App() {

    const [data, setData] = useState([]);
    const serverURL = "https://dizzon.herokuapp.com";

    useEffect(() => {
        
        Axios.get("https://dizzon.herokuapp.com/listShortURL").then((response) => {
            if (response.data.length > 0) {
                setData(response.data);
                console.log(response.data);
            }
            else {
                console.log("initial fetching failed");
            }
        });
        
    }, []);

    Axios.defaults.withCredentials = true;

    return <div className="App">
        <div class="container">
        <h1>URL Shortener</h1>
        <form action={serverURL+"/api/insertShortURL"} method="POST" class="my-4 form-inline">
            <label for="fullUrl" class="sr-only">Url</label>
            <input required placeholder="url" type="url" name="fullURL" id="fullURL" class="form-control mr-2 col"></input>
            <button type="submit" class="btn btn-success">Shrink</button>
        </form>
        <table class="table table-striped table-responsive ">
            <thead>
                <tr>
                    <th>Full URL</th>
                    <th>Shorten URL link</th>
                </tr>
            </thead>
            <tbody>
                {data.map((element,i) => {
                    return <tr>
                        <td><a href={element.fullURL}>{element.fullURL}</a></td>
                        <td><a href={serverURL+"/"+element.shortURL}>{serverURL+"/"+element.shortURL}</a></td>
                    </tr>
                })}
                    
                
            </tbody>
        </table>
    </div>
    </div>
    
    
}

export default App;
